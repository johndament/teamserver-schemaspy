#!/bin/bash

# Get config vars from env or defaults.
DB_HOST=${SCHEMASPY_HOST:-127.0.0.1}
DB_PORT=${SCHEMASPY_PORT:-3306}
DB_NAME=${SCHEMASPY_DB_NAME:-contrast}
DB_SCHEMA_NAME=${SCHEMASPY_SCHEMA_NAME:-contrast}
DB_USER=${SCHEMASPY_USER:-root}
DB_PASSWORD=${SCHEMASPY_PASSWORD:-""}

# Output config info.
echo "Generating schema for the following db settings:"
echo "================================================"
echo "host = $DB_HOST"
echo "port = $DB_PORT"
echo "user = $DB_USER"
echo "database name = $DB_NAME"
echo "database schema = $DB_SCHEMA_NAME"
echo " "


# Execute schemaspy to generate schema docs. See: https://schemaspy.readthedocs.io/en/latest/configuration/commandline.html
java -jar ./bin/schemaspy-6.1.0.jar \
	-t mysql \
	-host "$DB_HOST" \
	-port "$DB_PORT" \
	-db "$DB_NAME" \
	-u "$DB_USER" \
	-p "$DB_PASSWORD" \
	-o ./output/ \
	-dp ./bin/mysql-connector-java-5.1.48.jar \
	-s "$DB_SCHEMA_NAME" \
	-hq \
	-nopages \
	-meta teamserver-meta.xml \
	-noimplied \
	-css ./schemaSpy.css \
	-vizjs

# Copy the custom CSS over because -css switch isn't working.
cp ./schemaSpy.css ./output/schemaSpy.css

echo " "
echo " "
echo "Open ./output/index.html to view the schema documentation."
echo " "
