# TeamServer Database Schema Documentation

# Purpose
This repo contains the database schema documentation for TeamServer. It also has the code to re-generate that docuementation from an existing TeamServer database instance. 

The documentation includes tables, fields, indexes, constraints and diagrams. It also includes custom comments explaining the schema and its expected use.

The tool used to generate the documentation is SchemaSpy. See: https://schemaspy.readthedocs.io/en/latest/index.html

*If you have questions on this project, please contact Chris Edwards via email (chris.edwards@contrastsecurity.com) or slack (@chris.edwards).*

# Usage
There are 2 use cases.

* To view the most recently committed schema documentation, simply open the `./output/index.html` file.
* To re-generate the schema documentation (to include new changes to db schema or document comments), run `./generate.sh`.

# Custom Comments
SchemaSpy allows you to add custom comments to the schema when it is generated. These comments are specified in the `./teamserver-meta.xml` file.

Feel free to add to the documentation to clarify the schema for others. You can find documentation for using the xml file here: https://schemaspy.readthedocs.io/en/latest/configuration/schemaMeta.html

Note, you only need to specify the fields in the xml file that you want to add specific comments to. The schema is auto-discovered, so you do not need to specify every table and field in the xml.

If you make changes to the comments, please regenerate the schema and commit the new comments along with the updated schema documentation. This way, anyone who wants to view the docs can simply clone the repo and open the html. They don't have to run the generate script against an actual instance of the database.

# Configuration
The following environment variables will allow you to configure which database to connect to when running `./generate.sh`. By default it will run against a local MySQL instance.

* `SCHEMASPY_HOST` - The host the db is running on. Default is `127.0.0.1`
* `SCHEMASPY_PORT` - The port the db is listening on. Default is `3306`
* `SCHEMASPY_DB_NAME` - The name of the database to connect to. Default is `contrast`
* `SCHEMASPY_SCHEMA_NAME` - The schema name to generate documentation for. Default is `contrast`
* `SCHEMASPY_USER` - The username to connect to the db. Default is `root`
* `SCHEMASPY_PASSWORD` - Password to connect to db. Default is an empty password.
